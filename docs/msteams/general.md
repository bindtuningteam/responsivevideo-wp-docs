On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Video Options](./options)
- [Advanced Options](./advanced.md)
- [Web Part Messages](./message.md)

![06.options](../images/msteams/06.options.png)
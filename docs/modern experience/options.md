### Video Provider

Select the Platform that you intend to use to show the video. BindTuning provides 10 diferent providers by default and a generic where you can add just a simple embeded section.

![edit-page](../images/modern/03.videooptions.png)

### Video URL

After selection the video provider in the previous step, you can now add the URL of the Plarform that you intend to use to show the video. Below are the example of the URL that the Responsive Videos BindTuning Web Part will show:

##### BrightCove
 
Get the URL of the video like the example below:
- players.brightcove.net/XXXXXXXXXXXXXXXXXXX/default_default/index.html?videoId=XXXXXXXXXXXXXXXXXXX

##### Generic

With this option active the Video will add and iframe on the selected place. 

##### Microsoft Stream

Get the URL of the video like the example below:

- https://web.microsoftstream.com/video/b75685db-ba81-4eb4-a781-6aa4c44d1bc2
- https://web.microsoftstream.com/channel/5129174d-d1d5-4c53-a89a-23424693228a

Note this option will work with the Channels and Videos.

##### MP4 - Self Host

Get the URL of the video like the example below:

- http://clips.vorwaerts-gmbh.de/VfE_html5.mp4

##### Panopto

Get the URL of the video like the example below:

- https://corp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=69f90b29-6f14-444f-8ff2-9b615ed6c101
- https://corp.hosted.panopto.com/Panopto/Pages/Embed.aspx?id=69f90b29-6f14-444f-8ff2-9b615ed6c101

##### Vimeo

Get the URL of the video like the example below:

- https://vimeo.com/243244233

##### Video Portal

Get the URL of the video like the example below:

- https://bindtuning.sharepoint.com/portals/hub/_layouts/15/PointPublishing.aspx?app=video&p=p&chid=2bcf6591-9cd8-4a4c-a23f-1cab6da7d72c&vid=e2d7f4c6-c633-4955-a52f-14708b656fd9

##### Vidyard

Get the URL of the video like the example below:

-  Https://secure.vidyard.com/organizations/262005/embed_select/HJeLmAi1Bk3NHfDwJ19kyz
- http://play.vidyard.com/HJeLmAi1Bk3NHfDwJ19kyz
- https://video.vidyard.com/watch/HJeLmAi1Bk3NHfDwJ19kyz

##### Vzaar

Get the URL of the video like the example below:

- https://app.vzaar.com/videos/18052740 
- https://api.vzaar.com/videos/18052740/embed

##### Wistia

Get the URL of the video like the example below:

-  https://bindtuning-1.wistia.com/medias/yrcqse4j04
- https://fast.wistia.net/embed/iframe/yrcqse4j04

##### Youtube

Get the URL of the video like the example below:

- https://www.youtube.com/watch?v=69t4T2muJEU
- https://youtu.be/69t4T2muJEU

### Thumbnail URL

Add and URL for any specific image that you intend that acts like that thumbail in from of the video. When you click on that image, it'll start the video.

<p class="alert alert-info">If you just and image on this section the Web Part will act like a image provider instead of video.<p>